import sklearn
import numpy
from sklearn import svm
import spacy

nlp = spacy.load('en_core_web_lg')

input_files = 'state_tracker_input.txt'
states_purchase = ['card','cash','number','no', 'deliverytype','yes','cardnumber']
states_complain = ['complain_yes','complain_no']

clf  = svm.SVC()

output_files = 'states.txt'

input_lines = open(input_files,'r').readlines()
inputs = [word.strip('\n') for word in input_lines]

output_lines = open(output_files,'r').readlines()
outputs = [word.strip('\n') for word in output_lines]

output = []
for word in outputs:
    output.append(states_purchase.index(word))
    
input_vectors = []
for line in inputs:
    word = line.split(' ')
    vector_sum = 0
    for words in word:
        vector_sum += nlp.vocab[words].vector
    input_vectors.append(vector_sum)

clf.fit(input_vectors,output)


test_files = 'state_tracker_test.txt'
test_lines = open(test_files,'r').readlines()
tests = [word.strip('\n') for word in test_lines]


test_vectors = []
for line in tests:
    word = line.split(' ')
    vector_sum = 0
    for words in word:
        vector_sum += nlp.vocab[words].vector
    test_vectors.append(vector_sum)
    

state_s = clf.predict(test_vectors)

previous_state = {'card':0,'cash':0,'amount':0,'no':0, 'deliverytype':0,'yes':0,'cardnumber':0,'cardnumber_no':0,'amount_no':0,'deliverytype_no':0,'cardnumber_yes':0,'amount_yes':0,'deliverytype_yes':0}

for state in state_s:
    real_state = None
    if state == 0:
            real_state = 'card'
            previous_state['card'] = 1
    elif state ==  1:
            real_state = 'cash'
            previous_state['cash'] = 1
    elif state ==  2:
            if previous_state['cardnumber']:
                real_state = 'amount'
                previous_state['amount'] = 1            
            elif previous_state['cash']:
                real_state = 'amount'
                previous_state['amount'] = 1
            elif previous_state['card']:
                real_state = 'cardnumber'
                previous_state['cardnumber']  = 1
            else:
                pass
    elif state ==  3 :
            if previous_state['deliverytype']:
                real_state = 'deliverytype_no'
                previous_state['deliverytype_no'] = 1        
            elif previous_state['amount']:
                real_state = 'amount_no'
                previous_state['amount_no'] = 1
            elif previous_state['cardnumber']:
                real_state  = 'cardnumber_no'
                previous_state['cardnumber_no'] = 1
            else:
                pass
    elif state ==  4:
            if (previous_state['cash'] and previous_state['amount']) or (previous_state['card'] and previous_state['amount'] and previous_state['cardnumber']):
                real_state = 'deliverytype'
                previous_state['deliverytype'] = 1
            else:
                pass

    elif state ==  5:
            if previous_state['deliverytype']:
                real_state = 'deliverytype_yes'
                previous_state['deliverytype_yes'] = 1   
            elif previous_state['amount']:
                real_state = 'amount_yes'
                previous_state['amount_yes'] = 1
            elif previous_state['cardnumber']:
                real_state  = 'cardnumber_yes'
                previous_state['cardnumber_yes'] = 1         
            else:
                pass
    elif state ==  6:
            if previous_state['card']:
                real_state = 'cardnumber'
                previous_state['cardnumber'] = 1

    print(real_state)
